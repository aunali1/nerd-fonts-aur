# Nerd Fonts AUR

This project exists to simplify (re)packaging the Nerd Fonts upstream releases
to be less bandwidth intensive for AUR users. Currently this project only
repackages the "complete" release flavor. If I cannot manage to push packaging
changes upstream, I will eventually extend this project to individual fonts and
release flavors.

Contributions welcome!
